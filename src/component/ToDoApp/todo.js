import React, { useState, useEffect } from "react";
import "./style.css";

// GET LOCALSTORAGE TODO LIST

const getLocalToDoList = () => {
  const localToDoList = localStorage.getItem("myToDoList");

  if (localToDoList) {
    return JSON.parse(localToDoList);
  } else {
    return [];
  }
};

const ToDo = () => {
  const [toDo, setToDo] = useState("");
  const [toDoList, setToDoList] = useState(getLocalToDoList());
  const [isEditToDo, setIsEditToDo] = useState("");
  const [editButton, setEditButton] = useState(false);

  // TODOLIST FUNCTIONS

  // ADDTODO

  const addToDo = () => {
    if (!toDo) {
      alert("Oopsss... You Forgot To Add ToDo !");
    } else if (toDo && editButton) {
      setToDoList(
        toDoList.map((curElem) => {
          if (curElem.id === isEditToDo) {
            return { ...curElem, name: toDo };
          } else {
            return curElem;
          }
        })
      );
      setToDo("");
      setIsEditToDo(null);
      setEditButton(false);
    } else {
      const newTodo = {
        id: new Date().getTime().toString(),
        name: toDo,
      };

      setToDoList([...toDoList, newTodo]);
      setToDo("");
    }
  };

  // EDITTODO

  const editToDo = (index) => {
    const editedToDo = toDoList.find((curElem) => {
      return curElem.id === index;
    });
    setToDo(editedToDo.name);
    setIsEditToDo(index);
    setEditButton(true);
  };

  // DELETETODO

  const deleteToDo = (index) => {
    const updatedTodo = toDoList.filter((curElem) => {
      alert("You're ToDo List will be vanished !");
      return curElem.id !== index;
    });
    setToDoList(updatedTodo);
  };

  // DELETE TODOLIST

  const removeAll = () => {
    // setToDoList([]);
    if (getLocalToDoList().length === 0) {
      alert("Nothing to remove ....");
    } else {
      alert("You're ToDo List will be vanished !");
      setToDoList([]);
    }
  };

  // USING LOCALSTORAGE

  useEffect(() => {
    localStorage.setItem("myToDoList", JSON.stringify(toDoList));
  }, [toDoList]);

  return (
    <>
      <div className="main-div">
        <div className="child-div">
          <figure>
            <img src="./images/todo.svg" alt="ToDoLogo" />
            <figcaption>Add Your List Here....</figcaption>
          </figure>
          <div className="addItems">
            <input
              type="text"
              placeholder="✍ Add ToDo ...."
              className="form-control"
              value={toDo}
              onChange={(e) => setToDo(e.target.value)}
            />
            {editButton ? (
              <i className="far fa-edit add-btn" onClick={addToDo}></i>
            ) : (
              <i className="fa fa-plus add-btn" onClick={addToDo}></i>
            )}
          </div>

          {/* SHOW ITEMS */}

          <div className="showItems">
            {toDoList.map((curElem) => {
              return (
                <div className="eachItem" key={curElem.id}>
                  <h3>{curElem.name}</h3>
                  <div className="todo-btn">
                    <i
                      className="far fa-edit add-btn"
                      onClick={() => editToDo(curElem.id)}
                    ></i>
                    <i
                      className="far fa-trash-alt add-btn"
                      onClick={() => deleteToDo(curElem.id)}
                    ></i>
                  </div>
                </div>
              );
            })}
          </div>

          {/* REMOVE BUTTON */}

          <div className="showItems">
            <button
              className="btn effect04"
              data-sm-link-text="REMOVE ALL"
              onClick={removeAll}
            >
              <span>CHECK LIST</span>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ToDo;
