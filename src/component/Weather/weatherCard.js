import React, { useState, useEffect } from "react";

const WeatherCard = ({
  temp,
  humidity,
  pressure,
  weatherInfo,
  name,
  speed,
  country,
  sunset,
}) => {
  const [weatherState, setWeatherState] = useState("");

  useEffect(() => {
    if (weatherInfo) {
      switch (weatherInfo) {
        case "Clouds":
          setWeatherState("wi-cloudy");
          break;
        case "Haze":
          setWeatherState("wi-day-haze");
          break;
        case "Sunny":
          setWeatherState("wi-day-sunny");
          break;
        case "Fog":
          setWeatherState("wi-fog");
          break;
        case "Mist":
          setWeatherState("wi-dust");
          break;
        case "Clear":
          setWeatherState("wi-night-clear");
          break;

        default:
          setWeatherState("wi-day-sunny");
          break;
      }
    }
  }, [weatherInfo]);

  //   CONVERSION OF TIME INTO SECONDS

  let sec = sunset;
  let date = new Date(sec * 1000);
  let sunSetTime = `${date.getHours()}:${date.getMinutes()}`;

  return (
    <>
      <article className="widget">
        <div className="weatherIcon">
          <i className={`wi ${weatherState}`}></i>
        </div>

        <div className="weatherInfo">
          <div className="temperature">
            <span>{temp}&deg;</span>
          </div>

          <div className="description">
            <div className="weatherCondition">{weatherInfo}</div>
            <div className="place">
              {name}, {country}
            </div>
          </div>
        </div>

        <div className="date">{new Date().toLocaleString()}</div>

        {/* COLUMN SECTION */}

        <div className="extra-temp">
          <div className="temp-info-minmax">
            <div className="two-sided-section">
              <p>
                <i className={"wi wi-sunset"}></i>
              </p>
              <p className="extra-info-leftside">
                {sunSetTime} PM
                <br />
                Sunset
              </p>
            </div>

            <div className="two-sided-section">
              <p>
                <i className={"wi wi-humidity"}></i>
              </p>
              <p className="extra-info-leftside">
                {humidity}
                <br />
                Humidity
              </p>
            </div>
          </div>

          <div className="weather-extra-info">
            <div className="two-sided-section">
              <p>
                <i className={"wi wi-rain"}></i>
              </p>
              <p className="extra-info-leftside">
                {pressure}
                <br />
                Pressure
              </p>
            </div>

            <div className="two-sided-section">
              <p>
                <i className={"wi wi-strong-wind"}></i>
              </p>
              <p className="extra-info-leftside">
                {speed}
                <br />
                Speed
              </p>
            </div>
          </div>
        </div>
      </article>
    </>
  );
};

export default WeatherCard;
